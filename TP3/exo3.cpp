#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct SearchTreeNode : public Node
{    
    SearchTreeNode* left;
    SearchTreeNode* right;
    int value;

    void initNode(int value)
    {
        this->value = value;
        this->left = nullptr;
        this->right = nullptr;
    }

    void insertNumber(int value) {
        if (value <= this->get_value()) {
            if (this->get_left_child() == nullptr) {
                this->left = new SearchTreeNode(value);
            }
            else {
                this->get_left_child()->insertNumber(value);
            }
        }
        else {
            if (this->get_right_child() == nullptr) {
                this->right = new SearchTreeNode(value);
            }
            else {
                this->get_right_child()->insertNumber(value);
            }
        }

    }

    uint height() const	{
        if (this->left == nullptr && this->right == nullptr)
            return 1;
        else if (this->left == nullptr)
            return this->right->height()+1;
        else if (this->right == nullptr)
            return this->left->height()+1;
        if (this->right->height() > this->left->height())
            return this->right->height()+1;
        else return this->left->height()+1;
    }

	uint nodesCount() const {
        if (this->left == nullptr && this->right == nullptr)
            return 1;
        else if (this->left == nullptr)
            return this->right->nodesCount()+1;
        else if (this->right == nullptr)
            return this->left->nodesCount()+1;
        return this->right->nodesCount()+this->left->nodesCount()+1;
	}

	bool isLeaf() const {
        return (this->left == nullptr && this->right == nullptr);
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
        if (isLeaf()) {
            leaves[leavesCount++] = this;
            return;
        }
        if (this->right != nullptr) {
            this->right->allLeaves(leaves, leavesCount);
        }
        if (this->left != nullptr) {
            this->left->allLeaves(leaves, leavesCount);
        }
	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {
        if (this->isLeaf()) {
            nodes[nodesCount++] = this;
            return;
        }
        if (this->get_left_child() != nullptr)
            this->left->inorderTravel(nodes, nodesCount);
        nodes[nodesCount++] = this;
        if (this->get_right_child() != nullptr)
            this->right->inorderTravel(nodes, nodesCount);
	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        nodes[nodesCount++] = this;
        if (this->isLeaf())
            return;
        if (this->get_left_child() != nullptr)
            this->left->preorderTravel(nodes, nodesCount);
        if (this->get_right_child() != nullptr)
            this->right->preorderTravel(nodes, nodesCount);
	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        if (this->isLeaf()) {
            nodes[nodesCount++] = this;
            return;
        }
        if (this->get_left_child() != nullptr)
            this->left->postorderTravel(nodes, nodesCount);
        if (this->get_right_child() != nullptr)
            this->right->postorderTravel(nodes, nodesCount);
        nodes[nodesCount++] = this;
	}

	Node* find(int value) {
        if (value == this->value)
            return this;
        if (value < this->value && this->left != nullptr)
            return this->left->find(value);
        else if (value > this->value && this->right != nullptr)
            return this->right->find(value);
        else return nullptr;
	}

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    SearchTreeNode(int value) : Node(value) {initNode(value);}
    ~SearchTreeNode() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new SearchTreeNode(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<SearchTreeNode>();
	w->show();

	return a.exec();
}
