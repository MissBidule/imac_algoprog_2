#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant = nullptr;
};

struct Liste{
    Noeud* premier = nullptr;
};

struct DynaTableau{
    int* donnees = nullptr;
    size_t taille = 0;
    size_t capacite = 0;
};


void initialise(Liste* liste)
{
    liste->premier = nullptr;
}

bool est_vide(const Liste* liste)
{
    return !(liste->premier);
}

void ajoute(Liste* liste, int valeur)
{
    Noeud* temp;
    if (est_vide(liste)) {
        liste->premier = new Noeud;
        temp = liste->premier;
    }
    else {
        temp = liste->premier;
        while (temp->suivant != nullptr) {
            temp = temp->suivant;
        }
        temp->suivant = new Noeud;
        temp = temp->suivant;
    }
    temp->donnee = valeur;
}

void affiche(const Liste* liste)
{
    if (est_vide(liste)) {
        std::cout << "La liste est vide" << std::endl;
        return;
    }
    Noeud* temp = liste->premier;
    while (temp->suivant != nullptr) {
        std::cout << temp->donnee << std::endl;
        temp = temp->suivant;
    }
    std::cout << temp->donnee << std::endl;
}

int recupere(const Liste* liste, int n)
{
    if (est_vide(liste)) {
        std::cout << "La liste est vide" << std::endl;
        return 0;
    }
    Noeud* temp = liste->premier;
    int i;
    for (i = 0; i < n && temp->suivant != nullptr; i++) {
        temp = temp->suivant;
    }
    if (i == n) return temp->donnee;
    std::cout << "\nLe " << n << "ieme élément n'existe pas." << std::endl;
    return 0;
}

int cherche(const Liste* liste, int valeur)
{
    if (est_vide(liste))
        return -1;
    Noeud* temp = liste->premier;
    int i;
    for (i = 0; temp->suivant != nullptr && temp->donnee != valeur; i++) {
        temp = temp->suivant;
    }
    if (temp->donnee == valeur) return i;
    return -1;
}

void stocke(Liste* liste, int n, int valeur)
{
    if (est_vide(liste))
        return;
    Noeud* temp = liste->premier;
    int i;
    for (i = 1; temp->suivant != nullptr && i < n; i++) {
        temp = temp->suivant;
    }
    if (i == n) temp->donnee = valeur;
}

void ajoute(DynaTableau* tableau, int valeur)
{
    if (tableau->taille == tableau->capacite) {
        tableau->capacite *= 2;
        int* temp = new int[tableau->capacite];
        for (size_t i = 0; i < tableau->taille ; i++) {
            temp[i] = tableau->donnees[i];
        }
        delete [] tableau->donnees;
        tableau->donnees = temp;
    }
    tableau->donnees[tableau->taille++] = valeur;
}


void initialise(DynaTableau* tableau, int capacite)
{
    tableau->capacite = capacite;
    tableau->donnees = new int[capacite];
}

bool est_vide(const DynaTableau* tableau)
{
    return (tableau->taille == 0);
}

void affiche(const DynaTableau* tableau)
{
    if (est_vide(tableau)) {
        std::cout << "Le tableau est vide." << std::endl;
        return;
    }
    for (size_t i = 0; i < tableau->taille; i++) {
        std::cout << tableau->donnees[i] << std::endl;
    }
}

int recupere(const DynaTableau* tableau, int n)
{
    if ((uint)n-1 >= tableau->taille) {
        std::cout << "Le " << n << "ième élement n'existe pas." << std::endl;
        return 0;
    }
    return tableau->donnees[n];
}

int cherche(const DynaTableau* tableau, int valeur)
{
    size_t i;
    for (i = 0; i < tableau->taille  ; i++) {
        if (tableau->donnees[i] == valeur) {
            return i;
        }
    }
    std::cout << "La valeur n'existe pas dans le tableau." << std::endl;
    return 0;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    if ((uint)n >= tableau->taille) {
        std::cout << "Le " << n << "ième élement n'existe pas." << std::endl;
        return;
    }
    tableau->donnees[n] = valeur;
}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    ajoute(liste, valeur);
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{
    if (est_vide(liste)) {
        std::cout << "File vide." << std::endl;
        return 0;
    }

    Noeud* temp = liste->premier->suivant;
    int tempI = liste->premier->donnee;
    delete liste->premier;
    liste->premier = temp;
    return tempI;
}

void pousse_pile(DynaTableau* liste, int valeur)
//void pousse_pile(Liste* liste, int valeur)
{
    ajoute(liste, valeur);
}

int retire_pile(DynaTableau* liste)
//int retire_pile(Liste* liste)
{
    if (est_vide(liste)) {
        std::cout << "Pile vide." << std::endl;
        return 0;
    }
    return liste->donnees[--liste->taille];
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    //Liste pile;
    DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile, 10);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
