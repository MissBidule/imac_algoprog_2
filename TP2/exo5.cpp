#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

void merge(Array& first, Array& second, Array& result);

void splitAndMerge(Array& origin)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
    if (origin.size() <= 1) {
        return;
    }

	// initialisation
	Array& first = w->newArray(origin.size()/2);
	Array& second = w->newArray(origin.size()-first.size());
	
	// split
    int index = 0;
    for (int i = 0; i < first.size(); i++) {
        first[i] = origin[index++];
    }
    for (int i = 0; i < second.size(); i++) {
        second[i] = origin[index++];
    }

	// recursiv splitAndMerge of lowerArray and greaterArray
    splitAndMerge(first);
    splitAndMerge(second);

	// merge
    merge(first, second, origin);
}

void merge(Array& first, Array& second, Array& result)
{
    bool sorted = true;
    int i = 0;
    int j = 0;
   for (int index = 0; index < result.size(); index++) {
       if (i < first.size() && j < second.size()) {
           if (first[i] < second[j])
               result[index] = first[i++];
           else
               result[index] = second[j++];
       }
       else if (i >= first.size())
           result[index] = second[j++];
       else
           result[index] = first[i++];
   }
}

void mergeSort(Array& toSort)
{
    splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}
