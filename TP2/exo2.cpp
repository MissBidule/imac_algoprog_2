#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;

void insertionSort(Array& toSort){
	Array& sorted=w->newArray(toSort.size());
    sorted[0] = toSort[0];
    size_t taille = 1;
    for (int i = 1; i < sorted.size(); i++) {
        int n = toSort[i];
        bool found = false;
        for (int j = 0; j <= taille && !found; j++) {
            if (sorted[j]>n) {
                for (int k = j+1; k <= taille && k < toSort.size() ; k++) {
                    sorted[k+1] = sorted[k];
                }
                sorted[j+1] = sorted[j];
                sorted[j] = n;
                found = true;
            }
            else sorted[taille] = n;
        }
        taille++;
    }
	
	toSort=sorted; // update the original array
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(insertionSort); // window which display the behavior of the sort algorithm
	w->show();

	return a.exec();
}
